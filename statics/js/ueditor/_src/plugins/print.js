/**
 * ��ӡ
 * @file
 * @since 1.2.6.1
 */

/**
 * ��ӡ
 * @command print
 * @method execCommand
 * @param { String } cmd �����ַ���
 * @example
 * ```javascript
 * editor.execCommand( 'print' );
 * ```
 */
UE.commands['print'] = {
    execCommand : function(){
        this.window.print();
    },
    notNeedUndo : 1
};

