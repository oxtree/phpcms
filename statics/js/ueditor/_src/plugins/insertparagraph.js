/**
 * �������
 * @file
 * @since 1.2.6.1
 */


/**
 * �������
 * @command insertparagraph
 * @method execCommand
 * @param { String } cmd �����ַ���
 * @example
 * ```javascript
 * //editor�Ǳ༭��ʵ��
 * editor.execCommand( 'insertparagraph' );
 * ```
 */

UE.commands['insertparagraph'] = {
    execCommand : function( cmdName,front) {
        var me = this,
            range = me.selection.getRange(),
            start = range.startContainer,tmpNode;
        while(start ){
            if(domUtils.isBody(start)){
                break;
            }
            tmpNode = start;
            start = start.parentNode;
        }
        if(tmpNode){
            var p = me.document.createElement('p');
            if(front){
                tmpNode.parentNode.insertBefore(p,tmpNode)
            }else{
                tmpNode.parentNode.insertBefore(p,tmpNode.nextSibling)
            }
            domUtils.fillNode(me.document,p);
            range.setStart(p,0).setCursor(false,true);
        }
    }
};

